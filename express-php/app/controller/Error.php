<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-21
 * Time: 11:32
 */
namespace app\controller;

class Error {
    public function __call($name, $arguments) {
        // TODO: Implement __call() method.
        //dump($name);    //方法
        //dump($arguments);   //参数
        $result = [
            'status' => config("status.controller_not_found"),
            'message' => "找不到该控制器",
            'result' => null
        ];
        return json($result, 400);
    }
}