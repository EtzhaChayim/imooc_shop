<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-21
 * Time: 1:12
 */
namespace app\controller;

use app\BaseController;

class Demo extends BaseController {
    public function show(){
        $result = [
            "status" => 1,
            "message" => "OK",
            "result" => [
                'id' => 1,
            ],
        ];
        $header = [
            "Token" => "e23gdt55",
        ];
        //json
        return json($result,201, $header);
    }

    public function request(){
        //第一种获取方式
        dump($this->request->param("abc",1,"intval"));
    }
}