<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-21
 * Time: 1:43
 */
namespace app\controller;

use app\BaseController;
use app\Request;
use think\facade\Request as Abc;

class Learn {
    //实现Request获取url里面的一些基本数据
    public function index(Request $request) {
        dump($request->param("abc"));   //第二种获取方式 通过方法的依赖注入去实现
        dump(input("abc"));     //第三种获取方式
        dump(request()->param("abc"));   //第四种获取方式
        dump(Abc::param("abc"));    //第五种获取方式
    }
}