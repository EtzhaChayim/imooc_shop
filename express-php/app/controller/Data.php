<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-22
 * Time: 2:10
 */
namespace app\controller;

use app\BaseController;
use think\facade\Db;
use app\common\model\mysql\Demo;

class Data extends BaseController {
    public function index() {
        //  通过门面模式来获取
        //$result = Db::table("mall_demo")->where("id", 2)->find();
        //排序
//        $result = Db::table("mall_demo")
//            ->order("id", "desc")
//            //->limit(0, 2)   //分页的逻辑
//            ->page(1, 1)
//            ->select();

        $result = Db::table("mall_demo")
            //->where("id", "=", 2)
            //->where("id", 2)
            //->where(["id" => 2])
            ->where("id", "in", "1,2")
//            ->where([
//                ["id", ">", 2],
//                ["category_id", "=", 3],
//            ])
            ->select();

        //  通过容器的方式来处理
        //$result = app("db")->table("mall_demo")->where("id", 2)->find();
        dump($result);
    }

    public function abc() {
        //第一种输出SQL的方式
        $result = Db::table("mall_demo")->where("id", 10)->fetchSql()->find();

        //第二种输出SQL的方式
        $result = Db::table("mall_demo")->where("id", 10)->find();
        echo Db::getLastSql();exit;
        dump($result);
    }

    public function demo() {
        $data = [
            "title" => "21Libra",
            "content" => "测试数据",
            "category_id" => 2,
            "status" => 1,
            "create_time" => time()
        ];
        //新增逻辑
        //$result = Db::table("mall_demo")->insert($data);
        //echo Db::getLastSql();

        //删除操作  一般是假删除 防止误操作 半年或一年后用离线脚本扫描数据进行删除操作
        //$result = Db::table("mall_demo")->where("id", 2)->delete();
        //echo Db::getLastSql();

        //更新操作
        $result = Db::table("mall_demo")->where("id", 3)->update(["title" => "ceshi"]);
        echo Db::getLastSql();
        dump($result);
    }

    public function model1() {
        $result = Demo::find(1);
        //dump($result);
        dump($result->toArray());   //直接输出数组
    }

    public function model2() {
        $modelObj = new Demo();
        $results = $modelObj->where("category_id", 3)
            ->limit(2)
            ->order("id", "desc")
            ->select();
        //print_r($results);
        foreach ($results as $result) {
            dump($result->content);
            dump($result['content']);
            dump($result->status_text); //打印中文的文字
        }
    }
}