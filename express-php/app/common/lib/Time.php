<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-29
 * Time: 18:55
 */
namespace app\common\lib;

class Time {
    /**
     * 失效时间
     * @param int $type
     * @return float|int
     */
    public static function userLoginExpiresTime($type = 2) {
        $type = !in_array($type, [1, 2]) ? 2 : $type;
        if($type == 1) {
            $day = 7;
        } elseif ($type == 2) {
            $day = 30;
        }
        return $day * 24 * 3600;
    }
}