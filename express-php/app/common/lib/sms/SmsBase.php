<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-27
 * Time: 16:21
 */
declare(strict_types=1);
namespace app\common\lib\sms;

interface SmsBase {
    public static function sendCode(string $phone, int $code);
}