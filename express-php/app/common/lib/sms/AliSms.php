<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-27
 * Time: 16:03
 */
declare(strict_types=1);
namespace app\common\lib\sms;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use think\facade\Log;
class AliSms implements SmsBase{

    /**
     * 阿里云发送短信验证码的场景
     * @param string $phone
     * @param int $code
     * @return bool
     * @throws ClientException
     */
    //  : bool 返回值为布尔类型 因为发送短信 只关注发送成功还是失败 用true或false代表
    public static function sendCode(string $phone, int $code) : bool{
        if(empty($phone) || empty($code)) {
            return false;
        }

        AlibabaCloud::accessKeyClient(config("aliyun.access_key_id"), config("aliyun.access_key_secret"))
            ->regionId(config("aliyun.region_id"))
            ->asDefaultClient();

        $templateParam = [
            "code" => $code,
        ];
        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host(config("aliyun.host"))
                ->options([
                    'query' => [
                        'RegionId' => config("aliyun.region_id"),
                        'PhoneNumbers' => $phone,
                        'SignName' => config("aliyun.sign_name"),
                        'TemplateCode' => config("aliyun.template_code"),
                        'TemplateParam' => json_encode($templateParam),
                    ],
                ])
                ->request();
            //记录日志
            Log::info("alisms-sendCode-{$phone}result".json_encode($result->toArray()));
            //print_r($result->toArray());
        } catch (ClientException $e) {
            //记录日志 记录在runtime/api/log目录下
            Log::error("alisms-sendCode-{$phone}ClientException".$e->getErrorMessage());
            return false;
            //echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            //记录日志
            Log::error("alisms-sendCode-{$phone}ServerException".$e->getErrorMessage());
            return false;
            ///echo $e->getErrorMessage() . PHP_EOL;
        }
        //  isset — 检测变量是否已设置并且非 NULL
        if(isset($result['Code']) && $result['Code'] == "OK") {
            return true;
        }
        return false;

    }
}