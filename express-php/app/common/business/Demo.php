<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-24
 * Time: 14:37
 */
namespace app\common\business;

use app\common\model\mysql\Demo as DemoModel;

class Demo {
    /**
     * business层  通过getDemoDataByCategoryId来获取数据
     * @param $categoryId
     * @param int $limit
     * @return array
     */
    public function getDemoDataByCategoryId($categoryId, $limit = 10) {
        $model = new DemoModel();
        $results = $model->getDemoDataByCategoryId($categoryId, $limit);
        if(empty($results)) {
            return [];
        }
        $categorys = config("category");
        foreach ($results as $key => $result) {
            $results[$key]['categoryName'] = $categorys[$result["category_id"]] ?? "其他";
            //  ??是PHP7的特性 $categorys[$result["category_id"]] ?? "其他";等同于
            //  isset($categorys[$result["category_id"]]) ? $categorys[$result["category_id"]] : "其他";
        }
        return $results;
    }
}