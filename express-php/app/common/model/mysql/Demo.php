<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-23
 * Time: 3:35
 */
namespace app\common\model\mysql;

use think\Model;

class Demo extends Model {
    //ThinkPHP6文档 获取器
    public function getStatusTextAttr($value, $data) {
        $status = [
            0 => "待审核",
            1 => "正常",
            2 => "删除"
        ];
        return $status[$data['status']];
    }

    //  模型内容抽离
    public function getDemoDataByCategoryId($categoryId, $limit = 10) {
        //  如果$categoryId是空的 直接返回空数组 不用再往下执行 减少I/O操作
        if(empty($categoryId)) {
            return [];
        }
        $results = $this->where("category_id", $categoryId)
            ->limit($limit)
            ->order("id", "desc")
            ->select()
            ->toArray();
        return $results;
    }
}