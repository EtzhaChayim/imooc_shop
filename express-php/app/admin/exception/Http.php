<?php
/**
 * 负责本模块下的逻辑
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-24
 * Time: 15:09
 */
namespace app\admin\exception;

use think\exception\Handle;
use think\Response;
use Throwable;

class Http extends Handle {

    public $httpStatus = 500;
    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        //  做个判断 判断一下我们这个对象 $e对象下面是否存在 getStatusCode方法
        if(method_exists($e, "getStatusCode")) {
            $httpStatus = $e->getStatusCode();
        }else {
            $httpStatus = $this->httpStatus;
        }
        //dump($e->getStatusCode());
        // 添加自定义异常处理机制
        //  这种场景只适合api模块 因为api模块只需要输出这种api数据格式就可以了
        return show(config("status.error"), $e->getMessage(), [], $httpStatus);

    }

}