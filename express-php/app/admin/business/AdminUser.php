<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-26
 * Time: 1:46
 */
namespace app\admin\business;

use app\common\model\mysql\AdminUser as AdminUserModel;

//  不建议直接返回api数据格式数据  不规范  api数据格式返回的场景是在最上层controller层
class AdminUser {
    public $adminUserObj = null;
    //  什么是构造方法(函数)？
    //  构造方法(函数)是类中的一个特殊方法
    //  当使用new操作符创建一个类的实例时
    //  构造方法将会自动调用
    //  其名称必须是__construct
    //  一个类中只能声明一个构造方法
    //  而且每次创建对象的时候都会去调用一次构造方法
    //  不能主动的调用这个方法
    //  所以通常用它执行一些有用的初始化任务
    //  该方法无返回值
    public function __construct() {
        $this->adminUserObj = new AdminUserModel();
    }
    public function login($data) {
        //$adminUserObj = new AdminUserModel();
        //$adminUser = $adminUserObj->getAdminUserByUsername($data['username']);
        //  优化后
//        $adminUser = $this->adminUserObj->getAdminUserByUsername($data['username']);
//        //halt($adminUser);
//        if(empty($adminUser) || $adminUser->status != config("status.mysql.table_normal")) {
//            //return show(config("status.error"), "该用户不存在");
//            throw new \think\Exception("该用户不存在");
//        }
//        $adminUser = $adminUser->toArray();
        //  再次抽离
        $adminUser = $this->getAdminUserByUsername($data['username']);
        if(!$adminUser) {
            throw new \think\Exception("该用户不存在");
        }

        //  判断密码是否正确
        if($adminUser['password'] != md5($data['password']."_singwa_abc")) {
            //return show(config("status.error"), "密码错误");
            throw new \think\Exception("密码错误");
        }
        //需要记录信息到mysql表中
        $updateData = [
            "update_time" => time(),
            "last_login_time" => time(),
            //"last_login_ip" => $this->request->ip(),
            "last_login_ip" => request()->ip(),
        ];
        //$res = $adminUserObj->updateById($adminUser['id'], $updateData);
        //  优化后
        $res = $this->adminUserObj->updateById($adminUser['id'], $updateData);
        if(empty($res)) {
            //return show(config("status.error"), "登录失败");
            throw new \think\Exception("登录失败");
        }

        //  记录session
        session(config("admin.session_admin"), $adminUser);
        //return show(config("status.success"), "登录成功");
        return true;
    }

    public function getAdminUserByUsername($username) {
        $adminUser = $this->adminUserObj->getAdminUserByUsername($username);
        //halt($adminUser);
        if(empty($adminUser) || $adminUser->status != config("status.mysql.table_normal")) {
            //return show(config("status.error"), "该用户不存在");
            //throw new \think\Exception("该用户不存在");
            return [];
        }
        $adminUser = $adminUser->toArray();
        return $adminUser;
    }
}