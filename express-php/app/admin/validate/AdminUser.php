<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-26
 * Time: 0:37
 */
namespace app\admin\validate;

use think\Validate;

class AdminUser extends Validate {
    //  ThinkPHP6文档   验证器
    protected $rule = [
        'username' => 'require',
        'password' => 'require',
        'captcha' => 'require|checkCaptcha',
    ];
    //  信息提示
    protected $message = [
        'username' => '用户名不能为空',
        'password' => '密码不能为空',
        'captcha' => '验证码不能为空',
    ];

    protected function checkCaptcha($value, $rule, $data = []) {
        if(!captcha_check($value)) {
            return "您输入的验证码错误！";
        }
        return true;
    }
}