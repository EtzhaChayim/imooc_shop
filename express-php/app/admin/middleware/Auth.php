<?php
/**
 * 中间件
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-24
 * Time: 15:32
 */
declare(strict_types = 1);
namespace app\admin\middleware;

class Auth {
    //  中间件相当于是会先去经过handle 然后做完之后再去处理控制器里相应的场景
    //  中间件的入口执行方法必须是handle方法，而且第一个参数是Request对象，第二个参数是一个闭包
    //  ThinkPHP6文档 中间件
    public function handle($request, \Closure $next) {
        //  前置中间件
        //return $next($request);
        //  前置中间件获取不到$request->controller()方法
        //dump($request->pathinfo());
        //dump($request->controller());
        if(empty(session(config("admin.session_admin"))) && !preg_match("/login/", $request->pathinfo())) {
            //return redirect(url('login/index'));
            //  开启强制类型后 url需要转换成字符串
            return redirect((string)url('login/index'));
        }
        //  后置中间件  问题 会把controller的流程全都走一遍
        $response = $next($request);
//        if(empty(session(config("admin.session_admin"))) && $request->controller() != "Login") {
//            //return redirect(url('login/index'));
//            //  开启强制类型后 url需要转换成字符串
//            return redirect((string)url('login/index'));
//        }
        return $response;

    }

    /**
     * 中间件结束调度
     * @param \think\Response $response
     */
    public function end(\think\Response $response) {
        //  比方说有一些场景 在它结束之后可以打一些相应的日志
    }
}