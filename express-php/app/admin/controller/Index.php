<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-25
 * Time: 19:08
 */
namespace app\admin\controller;

use think\facade\View;

class Index extends AdminBase {

    public function index() {
        return View::fetch();
    }

    public function welcome() {
        return View::fetch();
    }
}