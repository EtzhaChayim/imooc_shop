<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-25
 * Time: 15:54
 */
namespace app\admin\controller;

use think\facade\View;
use app\common\model\mysql\AdminUser;

class Login extends AdminBase {
    //  如果已登录  则跳转到后台首页
    public function initialize() {
        if($this->isLogin()) {
            return $this->redirect(url("index/index"));
        }
    }

    public function index() {
        return View::fetch();
    }

    public function md5() {
        //  加密盐设置为_singwa_abc
        halt(session(config("admin.session_admin")));
        echo md5("admin_singwa_abc");
    }

    public function check() {
        //  判断请求是不是POST请求
        if(!$this->request->isPost()) {
            return show(config("status.error"), "请求方式错误");
        }
        //  参数校验    1.原生方式  2.TP6验证机制
        //  trim() 函数移除字符串两侧的空白字符或其他预定义字符
        $username = $this->request->param("username", "", "trim");
        $password = $this->request->param("password", "", "trim");
        $captcha = $this->request->param("captcha", "", "trim");
        //  前端做了校验为什么后端也要做  为了严谨  ajax请求会把请求地址暴露出去  可以模仿请求来走通后端逻辑
        //  抽离到validate层
//        if(empty($username) || empty($password) || empty($captcha)) {
//            return show(config("status.error"), "参数不能为空");
//        }
        $data = [
            'username' => $username,
            'password' => $password,
            'captcha' => $captcha,
        ];
        $validate = new \app\admin\validate\AdminUser();
        if(!$validate->check($data)) {
            return show(config("status.error"), $validate->getError());
        }

        //  抽离到validate层
        //  需要校验验证码是否正确
//        if(!captcha_check($captcha)) {
//            //  验证码校验失败
//            return show(config("status.error"), "验证码错误");
//        }
        //  抽离到business层
//        try{
//            $adminUserObj = new AdminUser();
//            $adminUser = $adminUserObj->getAdminUserByUsername($username);
//            //halt($adminUser);
//            if(empty($adminUser) || $adminUser->status != config("status.mysql.table_normal")) {
//                return show(config("status.error"), "该用户不存在");
//            }
//            $adminUser = $adminUser->toArray();
//            //  判断密码是否正确
//            if($adminUser['password'] != md5($password."_singwa_abc")) {
//                return show(config("status.error"), "密码错误");
//            }
//            //需要记录信息到mysql表中
//            $updateData = [
//                "update_time" => time(),
//                "last_login_time" => time(),
//                //"last_login_ip" => $this->request->ip(),
//                "last_login_ip" => request()->ip(),
//            ];
//            $res = $adminUserObj->updateById($adminUser['id'], $updateData);
//            if(empty($res)) {
//                return show(config("status.error"), "登录失败");
//            }
//        } catch (\Exception $e) {
//            //  todo 记录日志 $e->getMessage();  不需要把异常暴露给用户
//            return show(config("status.error"), "内部异常,登录失败");
//        }
//        //  记录session
//        session(config("admin.session_admin"), $adminUser);
//        return show(config("status.success"), "登录成功");
        try {
            $adminUserObj = new \app\admin\business\AdminUser();
            $result = $adminUserObj->login($data);
        } catch (\Exception $e) {
            return show(config("status.error"), $e->getMessage());
        }

        if($result) {
            return show(config("status.success"), "登录成功");
        }
        return show(config("status.error"), $validate->getError());
    }
}