<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-25
 * Time: 18:39
 */
namespace app\admin\controller;

use think\captcha\facade\Captcha;

class Verify {

    public function index() {
        return Captcha::create("verify");
    }
}