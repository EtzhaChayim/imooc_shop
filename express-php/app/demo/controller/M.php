<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-23
 * Time: 15:05
 */
//  初学者做法 有很多弊端
//  非常不友好的  因为把model层的逻辑全部写在controller上了
//  下一次其他控制器也需要去返回这个数据 或者调取这些逻辑数据 会非常痛苦
namespace app\demo\controller;

use app\BaseController;
use app\common\business\Demo;

class M extends BaseController {

    public function index() {
        $categoryId = $this->request->param("category_id", 0, "intval");
        //  intval的意义 过滤  因为category_id是一个int类型 所以必须做一个转换 否则非法用户可以SQL注入
        if(empty($categoryId)) {
            return show(config("status.error"), "参数错误");
        }
        
        //
        $model = new Demo();
        $results = $model->where("category_id", $categoryId)
            ->limit(10)
            ->order("id", "desc")
            ->select();
        //halt($results); //  等同 dump(); exit();

        //  $results返回的是一个对象 但对象的内容还是存在的 只是它的数据是空的 这样写不会往下走
//        if(empty($results)) {
//            return show(config("status.error"), "数据为空");
//        }
        //  把对象转换为数组  这个场景 思想一定要明确
        if(empty($results->toArray())) {
            return show(config("status.error"), "数据为空");
        }
        $categorys = config("category");
        foreach ($results as $key => $result) {
            $results[$key]['categoryName'] = $categorys[$result["category_id"]] ?? "其他";
            //  ??是PHP7的特性 $categorys[$result["category_id"]] ?? "其他";等同于
            //  isset($categorys[$result["category_id"]]) ? $categorys[$result["category_id"]] : "其他";
        }
        return show(config("status.success"), "ok", $results);
    }

    //  模型内容抽离
    public function index2() {
        $categoryId = $this->request->param("category_id", 0, "intval");
        //  intval的意义 过滤  因为category_id是一个int类型 所以必须做一个转换 否则非法用户可以SQL注入
        if(empty($categoryId)) {
            return show(config("status.error"), "参数错误");
        }

        //
        $model = new Demo();
        $results = $model->getDemoDataByCategoryId($categoryId);
        //halt($results); //  等同 dump(); exit();

        //  $results返回的是一个对象 但对象的内容还是存在的 只是它的数据是空的 这样写不会往下走
//        if(empty($results)) {
//            return show(config("status.error"), "数据为空");
//        }
        //  把对象转换为数组  这个场景 思想一定要明确
        if(empty($results)) {
            return show(config("status.error"), "数据为空");
        }
        $categorys = config("category");
        foreach ($results as $key => $result) {
            $results[$key]['categoryName'] = $categorys[$result["category_id"]] ?? "其他";
            //  ??是PHP7的特性 $categorys[$result["category_id"]] ?? "其他";等同于
            //  isset($categorys[$result["category_id"]]) ? $categorys[$result["category_id"]] : "其他";
        }
        return show(config("status.success"), "ok", $results);
    }

    //  business  业务层抽离
    public function index3() {
        $categoryId = $this->request->param("category_id", 0, "intval");
        //  intval的意义 过滤  因为category_id是一个int类型 所以必须做一个转换 否则非法用户可以SQL注入
        if(empty($categoryId)) {
            return show(config("status.error"), "参数错误");
        }
        $demo = new Demo();
        $results = $demo->getDemoDataByCategoryId($categoryId);
        return show(config("status.success"), "ok", $results);
    }
}