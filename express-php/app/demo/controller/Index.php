<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-23
 * Time: 4:27
 */
namespace app\demo\controller;

use app\BaseController;

class Index extends BaseController {

    public function abc() {
        return 123;
    }

    public function hello() {
        return time();
    }
}