<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-24
 * Time: 14:54
 */
namespace app\demo\controller;

use app\BaseController;

//  不可预知的内部异常处理
class E extends BaseController {

    public function index() {
        echo $abc;
    }

    public function index2() {
        throw new \think\exception\HttpException(401, "找不到相应的数据");
    }

    //  使用中间件
    public function abc() {
        dump($this->request->type);
    }
}