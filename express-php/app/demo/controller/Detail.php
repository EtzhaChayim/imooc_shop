<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-24
 * Time: 15:56
 */
namespace app\demo\controller;

use app\BaseController;

//  不可预知的内部异常处理
class Detail extends BaseController {
    //  需要注意 一般情况下需要在middleware.php里做配置  但是如果已在路由绑定 则不需要
    public function index() {
        dump($this->request->type);
    }

}