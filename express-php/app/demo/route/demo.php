<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-23
 * Time: 4:31
 */
use think\facade\Route;

Route::rule("test", "index/hello", "GET");
//  可以在路由的地方再次处理中间件
Route::rule("detail", "detail/index", "GET")->middleware(\app\demo\middleware\Detail::class);