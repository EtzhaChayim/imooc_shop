<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-24
 * Time: 15:54
 */
namespace app\demo\middleware;

class Detail {
    //  中间件相当于是会先去经过handle 然后做完之后再去处理控制器里相应的场景
    //  中间件的入口执行方法必须是handle方法，而且第一个参数是Request对象，第二个参数是一个闭包
    public function handle($request, \Closure $next) {
        //  可以处理一些相应的请求 公告的参数 拦截的请求
        $request->type = "detail";
        return $next($request);
    }

    /**
     * 中间件结束调度
     * @param \think\Response $response
     */
    public function end(\think\Response $response) {
        //  比方说有一些场景 在它结束之后可以打一些相应的日志
    }
}