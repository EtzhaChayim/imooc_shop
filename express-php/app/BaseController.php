<?php
declare (strict_types = 1);

namespace app;

use think\App;
use think\exception\ValidateException;
use think\Validate;

/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {}

    /**
     * 验证数据
     * @access protected
     * @param  array        $data     数据
     * @param  string|array $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement __call() method.
        //dump($name);    //方法
        //dump($arguments);   //参数
        //逻辑：如果我们的模块是api模块，需要输出api的数据格式
        //      如果我们是模板引擎的方式，需要输出错误页面
//        第一次通用化API数据格式数据优化,魔术方法__call()
//        魔术方法是PHP面向对象中特有的特性。它们在特定的情况下被触发，都是以双下划线开头，利用模式方法可以轻松实现PHP面向对象中重载
//        $result = [
//            'status' => 0,
//            'message' => "找不到该{$name}方法",
//            'result' => null
//        ];
//        return json($result,400);

//        第二次通用化API数据格式数据优化,通过app目录下的common文件调用
//        return show(100, "找不到该{$name}方法", null, 404);

//        第三次通用化API数据格式数据优化,通过config目录下的status文件存储业务状态码
        return show(config("status.action_not_found"), "找不到该{$name}方法", null, 404);
    }
}
