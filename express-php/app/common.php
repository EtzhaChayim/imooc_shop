<?php
// 应用公共文件
/**
 * 通用化API数据格式输出
 * @param $status
 * @param string $message
 * @param array $data
 * @param int $httpStatus
 * @return \think\response\Json
 */
//快捷键    /**+回车
//status业务状态码   message消息提示   data数据    httpStatus http状态码
function show($status, $message = "error", $data = [], $httpStatus = 200) {
    $result = [
        'status' => $status,
        'message' => $message,
        'result' => $data
    ];
    return json($result,$httpStatus);
}