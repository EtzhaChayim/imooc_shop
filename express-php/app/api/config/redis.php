<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-29
 * Time: 16:02
 */
return [
    "code_pre" => "mall_code_pre_", //短信验证码的前缀
    "code_expire" => 60,    //验证码失效时间
    "token_pre" => "mall_token_pre",
    "cart_pre" => "mall_cart_",
    // 延迟队列 - 订单是否需要取消状态检查
    "order_status_key" => "order_status",
    "order_expire" => 20*60,
];