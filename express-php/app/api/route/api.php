<?php
/**
 * Created by PhpStorm.
 * User: 21Libra
 * Date: 2020-11-27
 * Time: 16:23
 */
use think\facade\Route;

Route::rule("smscode", "sms/code", "POST");
Route::resource('user', 'User');    //  资源路由
Route::rule("lists", "mall.lists/index");
Route::rule("subcategory/:id", "category/sub");
Route::rule("detail/:id", "mall.detail/index");

Route::resource("order", "order.index");